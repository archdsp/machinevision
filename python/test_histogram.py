import numpy as np
import PIL.Image
import cv2
from matplotlib import pyplot as plt

import histogram

def compare_hist_with_opencv():
    img = np.asarray(PIL.Image.open('../test_imgs/lena.jpg').convert('L'), 'uint8')
    img2 = cv2.equalizeHist(img)
    img3 = histogram.histogram_equalization(img)

    # Original img's histogram
    hist1, bin_edges = histogram.histogram(img.ravel(), 256, (0, 255), True)

    # Original cv2's equalized img's histogram
    hist2, bin_edges = histogram.histogram(img2.ravel(), 256, (0, 255), True)
    
    # Original my equalized img's histogram
    hist3, bin_edges = histogram.histogram(img3.ravel(), 256, (0, 255), True)

    figure_row = 3

    # Plot original histogram
    plt.subplot(figure_row, 1, 1)
    plt.bar(bin_edges, hist1)

    # plot cv2's equalized img's histogram
    plt.subplot(figure_row, 1, 2)
    plt.bar(bin_edges, hist2)

    # plot my equalized img's histogram
    plt.subplot(figure_row, 1, 3)
    plt.bar(bin_edges, hist3)
    
    plt.show()

    
def test_hist_equalized_img():
    img = np.asarray(PIL.Image.open('../test_imgs/lena.jpg').convert('L'), 'uint8')
    ravelled_img = img.ravel()
    
    hist, bin_edges = histogram.histogram(ravelled_img, 256, (0, 255), True)
    c_hist = histogram.cumulative_histogram(hist)

    equalized_img = histogram.histogram_equalization(img)
    e_hist, _ = histogram.histogram(equalized_img.ravel(), 256, (0, 255), True)
    e_c_hist = histogram.cumulative_histogram(e_hist)

    plt.subplot(2, 2, 1)
    plt.xlabel("Width")
    plt.ylabel("Height")
    plt.title("Image")
    plt.imshow(img, cmap='gray')

    plt.subplot(2, 2, 3)
    plt.xlabel("Width")
    plt.ylabel("Height")
    plt.title("Equalized Image")
    plt.imshow(equalized_img, cmap='gray')

    plt.subplot(4, 2, 2)
    plt.xlabel("Intensity")
    plt.ylabel("Frequency")
    plt.title("Histogram")
    plt.bar(bin_edges, hist)

    plt.subplot(4, 2, 4)
    plt.xlabel("Intensity")
    plt.ylabel("Frequency")
    plt.title("Cumulative Histogram")
    plt.bar(bin_edges, c_hist)

    plt.subplot(4, 2, 6)
    plt.xlabel("Intensity")
    plt.ylabel("Frequency")
    plt.title("Equalized Histogram")
    plt.bar(bin_edges, e_hist)

    plt.subplot(4, 2, 8)
    plt.xlabel("Intensity")
    plt.ylabel("Frequency")
    plt.title("Equalized Cumulative Histogram")
    plt.bar(bin_edges, e_c_hist)

    plt.tight_layout()
    plt.show()
    

if __name__ == "__main__":
    #compare_hist_with_opencv()
    test_hist_equalized_img()
